var selector1 = '#menu_item1';
var selector2 = '#menu_item2';
var selector3 = '#menu_item3';
var selector4 = '#menu_item4';
var selector5 = '#menu_item5';
var selector6 = '#menu_item6';
var selector7 = '#menu_item7';
var selector8 = '#menu_item8';

$(document).ready(function() {
  $(".nav li a").removeClass("active");
});

$('.nav li a').on('click', function() {
  var scrollAnchor = $(this).attr('data-scroll'),
    scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 28;
  $('body,html').animate({
    scrollTop: scrollPoint
  }, 500);
  return false;
})


$(window).scroll(function() {
  var windscroll = $(window).scrollTop();
  
  var sec1 = $('#accueil').offset().top - 50;
  var sec2 = $('#profil').offset().top - 50;
  var sec3 = $('#formations').offset().top - 50;
  var sec4 = $('#certifications').offset().top - 50;
  var sec5 = $('#competences').offset().top - 50;
  var sec6 = $('#masauvegarde').offset().top - 50;
  var sec7 = $('#izybat').offset().top - 50;
  var sec8 = $('#projets').offset().top - 50;
  var sec9 = $('#veille').offset().top - 50;

  if (windscroll >= sec1) {
    $(selector1).addClass('custom_active');
  } // do not remove this class

  if (windscroll >= sec2) {
    $(selector2).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
  } else {
    $(selector2).removeClass('custom_active');
  }

  if (windscroll >= sec3) {
    $(selector3).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
  } else {
    $(selector3).removeClass('custom_active');
  }

  if (windscroll >= sec4) {
    $(selector4).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
  } else {
    $(selector4).removeClass('custom_active');
  }

  if (windscroll >= sec5) {
    $(selector5).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
  } else {
    $(selector5).removeClass('custom_active');
  }

  if (windscroll >= sec6 || windscroll >= sec7) {
    $(selector6).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
    $(selector5).removeClass('custom_active');
  } else {
    $(selector6).removeClass('custom_active');
  }

  if (windscroll >= sec8) {
    $(selector7).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
    $(selector5).removeClass('custom_active');
    $(selector6).removeClass('custom_active');
  } else {
    $(selector7).removeClass('custom_active');
  }

  if (windscroll >= sec9) {
    $(selector8).addClass('custom_active');
    
    $(selector1).removeClass('custom_active');
    $(selector2).removeClass('custom_active');
    $(selector3).removeClass('custom_active');
    $(selector4).removeClass('custom_active');
    $(selector5).removeClass('custom_active');
    $(selector6).removeClass('custom_active');
    $(selector7).removeClass('custom_active');
  } else {
    $(selector8).removeClass('custom_active');
  }

}).scroll();
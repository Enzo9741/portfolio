var keyHandler = function (event) {
    if (event.key === 'ArrowLeft') {
        if(detect_visibility(document.getElementById('profil'))){
            window.scroll(0,findPos(document.getElementById("accueil")));
        }else if(detect_visibility(document.getElementById('formations'))){
            window.scroll(0,findPos(document.getElementById("profil")));     
        }else if(detect_visibility(document.getElementById('certifications'))){
            window.scroll(0,findPos(document.getElementById("formations")));
        }else if(detect_visibility(document.getElementById('competences'))){
            window.scroll(0,findPos(document.getElementById("certifications")));
        }else if(detect_visibility(document.getElementById('masauvegarde'))){
           window.scroll(0,findPos(document.getElementById("competences")));
        }else if(detect_visibility(document.getElementById('izybat'))){
            window.scroll(0,findPos(document.getElementById("masauvegarde")));
        }else if(detect_visibility(document.getElementById('projets'))){
            window.scroll(0,findPos(document.getElementById("izybat")));
        }else if(detect_visibility(document.getElementById('veille'))){
            window.scroll(0,findPos(document.getElementById("projets")));
        }
    }else if(event.key === 'ArrowRight'){
        if(detect_visibility(document.getElementById('accueil'))){
            window.scroll(0,findPos(document.getElementById("profil")));
        }else if(detect_visibility(document.getElementById('profil'))){
            window.scroll(0,findPos(document.getElementById("formations")));
        }else if(detect_visibility(document.getElementById('formations'))){
            window.scroll(0,findPos(document.getElementById("certifications")));
        }else if(detect_visibility(document.getElementById('certifications'))){
            window.scroll(0,findPos(document.getElementById("competences")));
        }else if(detect_visibility(document.getElementById('competences'))){
            window.scroll(0,findPos(document.getElementById("masauvegarde")));
        }else if(detect_visibility(document.getElementById('masauvegarde'))){
            window.scroll(0,findPos(document.getElementById("izybat")));
        }else if(detect_visibility(document.getElementById('izybat'))){
            window.scroll(0,findPos(document.getElementById("projets")));
        }else if(detect_visibility(document.getElementById('projets'))){
            window.scroll(0,findPos(document.getElementById("veille")));
        }
    }
};

function detect_visibility(element) {

    var top_of_element = element.offsetTop;
    var bottom_of_element = element.offsetTop + element.offsetHeight + element.style.marginTop;
    var bottom_of_screen = window.scrollY + window.innerHeight;
    var top_of_screen = window.scrollY;

    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
        //console.log("element is visible " + element.id)
        return true;
    } else {
        //console.log("element is not visible " + element.id)
        return false;
    }

}

function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return [curtop];
    }
}



document.addEventListener('keydown', keyHandler, false);

